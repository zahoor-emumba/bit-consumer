import React from "react";
import { Button } from "@remote-scope/ui.button";

function Card({ children, ...rest }) {
  return (
    <div {...rest}>
      {children}
      <Button
        onClick={() => alert("Installed From Verdaccio")}
        style={{ backgroundColor: "green", color: "white" }}
      >
        Show Alert
      </Button>
    </div>
  );
}

export default Card;
